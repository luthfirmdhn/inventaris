<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SARPRAS</title>
    <link rel="icon" type="images/css" href="img/lg.png">

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/agency.min.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="img/lg.png" width="10%" height="10%" alt="">  Sistem Informasi Sarana Dan Prasarana</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Beranda
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
<!--             <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services">Tentang</a>
            </li> -->
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#profil">Profil</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services">Tentang</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#portfolio">Galeri</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Kontak</a>
            </li>

          </ul>
        </div>
      </div>
    </nav>

    <!-- Header -->
    <header class="masthead">
      <div class="container">
        <div class="intro-text">
          <div class="intro-lead-in"></div>
          <div class="intro-heading text-uppercase"></div>

          <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services">Selengkapnya</a>
        </div>
      </div>
    </header>

    <!-- profil -->
    <section id="profil">
      <div class="container">
        <div class="row">
            <div class="content-1">
                <div class="col-lg-12">
                    <h2 class="page-header" style="text-align: center";>Data Profil Sekolah</h2>
                </div>
                <table class="table table-striped" style="background-color: powderblue";>
                        <tr>
                            <th>NIP</th><th>:</th><th style="text-align: center;">196706012000031003</th>
                        </tr>
                        <tr>
                            <th>NPSN</th><th>:</th><th style="text-align: center;">20254135</th>
                        </tr>
                        <tr>
                            <th>NSS</th><th>:</th><th style="text-align: center;">401020229101</th>
                        </tr>
                        <tr>
                            <th>Nama Sekolah</th><th>:</th><th style="text-align: center;">SMK Negeri 1 Ciomas</th>
                        </th>
                        <tr>
                            <th>Akreditasi</th><th>:</th><th style="text-align: center;">Akreditasi A</th>
                        </th>
                        <tr>
                            <th>Jenjang</th><th>:</th><th style="text-align: center;">SMK</th>
                        </th>
                        <tr>
                            <th>Status</th><th>:</th><th style="text-align: center;">Negeri</th>
                        </th>
                        <tr>
                            <th>Tanggal Berdiri</th><th>:</th><th style="text-align: center;">25 Maret 2008</th>
                        </tr>
                        <tr>
                            <th>ID UN</th><th>:</th><th style="text-align: center;">258</th>
                        </tr>

                        <tr>
                            <th>Email</th><th>:</th><th style="text-align: center;">smkn1_ciomas@yahoo.co.id</th>
                        </tr>
                        <tr>
                            <th>Kepala Sekolah</th><th>:</th><th style="text-align: center;">Miswan Wahyudi, MM</th>
                        </tr>
                        <tr>
                            <th>Alamat Sekolah</th><th>:</th><th style="text-align: center;">Jl. Laladon Desa Laladon Kab.Bogor Barat</th>
                        </tr>
                        <tr>
                            <th>Kecamatan</th><th>:</th><th style="text-align: center;">Ciomas</th>
                        </tr>
                        <tr>
                            <th>Kota</th><th>:</th><th style="text-align: center;">Bogor</th>
                        </tr>
                        <tr>
                            <th>Kode Pos</th><th>:</th><th style="text-align: center;">16710</th>
                        </tr>
                        <tr>
                            <th>Provinsi</th><th>:</th><th style="text-align: center;">Jawa Barat</th>
                        </tr>
                        <tr>
                            <th>Telepon</th><th>:</th><th style="text-align: center;">0251-7520-933</th>
                        </tr>
                </table>
            </div>
    </section>

    <!-- Services -->
    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
                        <h4 class="service-heading" align="center">Tentang</h4>
    <p align="center"><font color="black" size="10pt"><b>S</b></font><font size="5pt">arana pendidikan adalah segala macam peralatan yang digunakan guru untuk memudahkan penyampaian materi pembelajaran.Jika dilihat dari sudut murid,sarana pendidikan adalah segala macam peralatan yang digunakan murid untuk mempelajari mata pelajaran</font></p><br>
    <p align="center"><font color="black" size="10pt"><b>P</b></font><font size="5pt">rasarana pendidikan adalah segala macam peralatan,kelengkapan,dan benda-benda yang digunakan guru dan murid untuk memudahkan penyelenggaraan pendidikan</font></p>
          </div>
        </div>
        <div class="row text-center">
       <h4 class="service-heading" style="font-color:#fed136";>Jurusan</h4>
<div>
     <img src="img/rpl.jpg" width="15%" height="20%" align="left">
    <p align="left" ><font color="white" size="6" >Rekayasa Perangkat Lunak</font><br>
    Rekayasa perangkat lunak(RPL) atau dalam bahasa Inggris yaitu Software Engineering(SE) adalah salah satu bidang di Teknik Informatika yang bergerak dalam pengembangan software (perangkat lunak). Perangkat Lunak yang dikembangkan dapat berupa; Perangkat Lunak Berbasis Desktop,Perangkat Lunak Berbasis Web,Perangkat Lunak Berbasis Mobile.</p><br>

    <img class="lgtentang" src="img/anm.jpg" width="15%" height="20%" align="right">
    <p align="right" ><font color="white" size="6" >Animasi</font><br>
    ANIMASI adalah penggunaan komputer untuk menyajikan dan menggabungkan teks,suara,gambar, animasi dan video dengan alat bantu dan koneksi sehingga pengguna dapat berinteraksi,berkarya dan berkomunikasi.Multimedia sering digunakan dalam dunia hiburan.</p><br>

    <img class="lgtentang" src="img/tkr.jpg" width="15%" height="20%" align="left">
    <p align="left" ><font color="white" size="6" >Teknik Kendaraan Ringan</font><br>
   TKR Pendidikan praktek Jurusan teknik mesin di SMK dikategorikan masih umum, banyak sekali yang masih menjurus seperti: Otomotif, Mesin produksi, Las dll.Tetapi kebanyakan disekolah SMK yang ada di Indonesia adalah Jurusan Otomotifnya untuk jurusan teknik mesin.</p><br>

    <img class="lgtentang" src="img/tpl.jpg" width="15%" height="20%" align="right">
    <p align="right" ><font color="white" size="6" >Teknik Pengelasan</font><br>
    Pengelasan (welding) adalah salah salah satu teknik penyambungan logam dengan cara mencairkan sebagian logam induk dan logam pengisi dengan atau tanpa tekanan dan dengan atau tanpa logam penambah.</p><br>
    </section>
  </div>
    <!-- Portfolio Grid -->
    <section class="bg-light" id="portfolio">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Galeri Dan Fasilitas</h2>
            <h3 class="section-subheading text-muted">Berikut Tampilan Gambar Fasilitas Yang Tersedia</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal1">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/10rpl1.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Kelas</h4>
              <p class="text-muted">Tempat berlangsungnya kegiatan belajar mengajar </p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/lab.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Lab RPL</h4>
              <p class="text-muted">Tempat praktikum RPL</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal3">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/bengkel.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Bengkel</h4>
              <p class="text-muted">Tempat praktikum TKR dan TPL</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal4">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/mushola.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Kantor Guru</h4>
              <p class="text-muted">Tempat khusus guru</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal5">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/mushola.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Mushola</h4>
              <p class="text-muted">Tempat beribadah</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal6">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/toilet.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Toilet</h4>
              <p class="text-muted">Tempat buang air</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/studioanm.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Studio ANIMASI</h4>
              <p class="text-muted">Tempat praktikum ANIMASI</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/posatpam.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Pos Satpam</h4>
              <p class="text-muted">Tempat para satpam</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/ruangosis.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Ruang Osis</h4>
              <p class="text-muted">Ruangan khusus osis</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/tu.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Tata Usaha</h4>
              <p class="text-muted">Tempat Administrasi,Penyimpanan Data Siswa dan Sekolah</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/mushola.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Ruang Kepsek</h4>
              <p class="text-muted">Ruangan khusus kepala sekolah</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/bk.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Ruang BK</h4>
              <p class="text-muted">Ruangan khusus siswa bermasalah</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/lapangan_bf.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Lapangan Basket & Futsal</h4>
              <p class="text-muted">Tempat praktikum olahraga</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/lapangan_voli.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Lapangan Voli</h4>
              <p class="text-muted">Tempat praktikum olahraga</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/parkiran.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Parkiran</h4>
              <p class="text-muted">Tempat untuk menyimpan kedaraan</p>
            </div>
          </div>
<!--           <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/kantin.jpg" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Kantin</h4>
              <p class="text-muted">Barisan para pejuang Indonesia</p>
            </div>
          </div> -->
        </div>
      </div>
    </section>

    <!-- Clients -->
<!--     <section class="py-5">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <a href="#">
              <img class="img-fluid d-block mx-auto" src="img/logos/envato.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-sm-6">
            <a href="#">
              <img class="img-fluid d-block mx-auto" src="img/logos/designmodo.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-sm-6">
            <a href="#">
              <img class="img-fluid d-block mx-auto" src="img/logos/themeforest.jpg" alt="">
            </a>
          </div>
          <div class="col-md-3 col-sm-6">
            <a href="#">
              <img class="img-fluid d-block mx-auto" src="img/logos/creative-market.jpg" alt="">
            </a>
          </div>
        </div>
      </div>
    </section> -->

    <!-- Contact -->
    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
                <table cellspacing="0" width="100%" cellpadding="40" id="kontak" border="0">
      <tr>
        <td width="25%" align="left"><font size="5"><span style="color:black;font-weight:bold;">Kontak</span></font>
        <br>
<span class="glyphicon glyphicon-earphone" aria-hidden="true"><width="25" height="25"><font color="black"> 02517520933</font></span>
        <br>
        <a href="" target="blank">
        <a href="" target="blank"><img src="img/fb.jpg" width="25" height="25"><font color="black"> SMKN 1 Ciomas</font>
        <br>
        <a href="" target="blank">
        <a href="" target="blank"><img src="img/ig.jpg" width="25" height="25"><font color="black"> SMKN 1 Ciomas</font>
        <br>
        <a href="https://mail.google.com/mail/#inbox" target="blank"><a href="" target="blank"></a><img src="img/email.jpg" width="25" height="25"></a><font color="black"> smkn1_ciomas@yahoo.co.id</font>
        <br>
        <a href="https://mail.google.com/mail/#inbox" target="blank"><img src="img/website.jpg" width="25" height="25"></a><font color=black size="5pt"> www.smkn1ciomas.sch.id</font>
        </td>
        <td width="25%" align="center"><font color="black" size="5"><span style="color:black;font-weight:bold;">Alamat</span></font>
        <p><img src="img/alamat.jpg" width="25" height="25"><font color="black">Jln.Laladon Desa Laladon Kec Ciomas Kab Bogor</font></p>
        </td>

        <td width="25%" align="center"><font color="black" size="5"><span style="color:black;font-weight:bold;">Tujuan</span></font>
        <p><font color="black">Dengan Adanya Website Sistem Informasi Sarana Dan Prasarana ini Semoga Dapat Bermanfaat Bagi Pengguna Yang Membutuhkan</font></p>
      </tr>
  </table>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <form id="contactForm" name="sentMessage" novalidate>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" id="name" type="text" placeholder="Masukan Nama Anda *" required data-validation-required-message="Please enter your name.">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="email" type="email" placeholder="Masukan Email Anda *" required data-validation-required-message="Please enter your email address.">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="phone" type="tel" placeholder="Masukan Nomor Telepon Anda *" required data-validation-required-message="Please enter your phone number.">
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <textarea class="form-control" id="message" placeholder="Masukan Pesan *" required data-validation-required-message="Please enter a message."></textarea>
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12 text-center">
                  <div id="success"></div>
                  <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">Kirim Pesan</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; 2018 sarpras.com</span>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-4">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                <a href="#">Privacy Policy</a>
              </li>
              <li class="list-inline-item">
                <a href="#">Terms of Use</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

    <!-- Portfolio Modals -->

    <!-- Modal 1 -->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
<!-- Project Details Go Here -->
                  <h2 class="text-uppercase">Kelas</h2>
                  <p class="item-intro text-muted">Tempat berlangsungnya kegiatan belajar mengajar</p>
                  <img class="img-fluid d-block mx-auto" src="img/10rpl1.jpg" alt="">
                  <table border="1" style="background-color: #009688; color: #fff; font-weight: bold;">
                    <th>No.</th>
                    <th>Semester</th>
                    <th>Ruangan</th>
                    <th>Nama Barang</th>
                    <th>No.Kode Barang</th>
                    <th>Jumlah Barang</th>
                    <th>Harga Beli</th>
                    <th>Ukuran</th>
                    <th>Baik</th>
                    <th>Kurang Baik</th>
                    <th>Rusak Berat</th>
                    <th>Layak Pakai</th>
                    <th>Gambar</th>

                    <?php
                    $query = mysqli_query ($kon, "SELECT * FROM 10rpl1");
                    while ($data = mysqli_fetch_array($query)){
                    ?>

                      <br><?php echo $data ['10rpl1']; ?>

                    <?php } ?>

                    <?php
                    $view = $kon --> ("SELECT * FROM 10rpl1");
                    $i = 1;
                    while($row = $view --> fetch_array())
                    {
                    ?>

                    <?php echo '<p> <img src="data:image/jpeg;base64, ' .base64_encode($row['10rpl1']). '" width="100%" height="200%"</p>'
                    ?>
                      <?php

                      ?>
                      </hr>
                    <?php
                  }
                  ?>


                  </table>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Tutup</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 2 -->
    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase">Gambar 2</h2>
                  <p class="item-intro text-muted">Barisan para pejuang Indonesia.</p>
                  <img class="img-fluid d-block mx-auto" src="img/2.jpg.jpg" alt="">
                  <p>Para pejuang Indonesia tentunya telah berjuang keras untuk mempertahankan kota Bandung agar tidak dijadikan markas strategis militer oleh para sekutu namun apa daya perbandingan kekuatan antara pasukan sekutu dan pejuang Indonesia sangat berbeda jauh dan akhirnya mereka memutuskan untuk membakar habis kota Bandung bagian utara.</p>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Tutup</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 3 -->
    <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase">Gambar 3</h2>
                  <p class="item-intro text-muted">Pejuang wanita yang ikut bertempur</p>
                  <img class="img-fluid d-block mx-auto" src="img/wanita.jpg" alt="">
                  <p>Setelah melihat para pejuang pria yang berjuang keras untuk mempertahankan kota Bandung tentunya pejuang wanita pun tidak hanya berdiam diri namun mereka ikut membantu untuk mengamani dan memindahkan masyarakat pasca pembakaran kota Bandung bagian utara.  </p>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Tutup</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 4 -->
    <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase">Gambar 4</h2>
                  <p class="item-intro text-muted">Suasana panik dan bingung yang dialami masyarakat kota Bandung pada saat itu.</p>
                  <img class="img-fluid d-block mx-auto" src="img/7.jpg" alt="">
                  <p>Suasana dan situasi kota Bandung semakin panik, genting dan mencekam. Pejuang di kota Bandung merasa kebingungan harus mengikuti instruksi mana, karena terdapat dua instruksi yang berlainan. Keputusan pun harus segera dibuat, akhirnya pejuang kemerdekaan memutuskan untuk melakukan serangan dengan sekala besar terhadap pasukan sekutu. </p>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Tutup</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 5 -->
    <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase">Gambar 5</h2>
                  <p class="item-intro text-muted">Hangusnya seluruh kota Bandung bagian Utara</p>
                  <img class="img-fluid d-block mx-auto" src="img/6.jpg" alt="">
                  <p>Hangusnya seluruh kota Bandung di bagian utara ini merupakan tujuan dari para pejuang Indonesia agar wilayah ini tidak direbut oleh sekutu</p>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Tutup</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 6 -->
    <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase">Gambar 6</h2>
                  <p class="item-intro text-muted">Monumen Bandung Lautan Api</p>
                  <img class="img-fluid d-block mx-auto" src="img/bla.jpg" alt="">
                  <p>Dibuatnya Monumen Bandung Lautan Api kemudian menjadi salah satu ciri khas kota tersebut. Monumen Bandung Lautan Api memiliki sisi sembilan dan tingginya 25 meter. Dibangunnya monumen ini bertujuan untuk memperingati aksi "Bandung Lautan Api". Monumen ini lokasinya berada dikawasan Lap. Tegallega, tepatnya di tengah-tengah kota dan monumen yang cukup terkenal di kota Bandung. Setiap tanggal 23 Maret, monumen ini ramai dikunjungi karena untuk mengenang perjuangan saat peristiwa Bandung Lautan Api berlangsung.</p>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Tutup</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/agency.min.js"></script>

  </body>

</html>
