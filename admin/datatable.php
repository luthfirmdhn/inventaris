<?php
include "koneksi.php";
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>AdminLTE 2 | Data Tables</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Data Table With Full Features</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example" class="table table-bordered table-striped">
        <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Kondisi</th>
                                        <th>Keterangan</th>
                                        <th>Ruang</th>
                                        <th>Jumlah</th>
                                        <th>Jenis</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $data=mysql_query("SELECT * FROM inventaris i JOIN ruang r ON i.id_ruang=r.id_ruang JOIN jenis j ON i.id_jenis=j.id_jenis");
                                $no=1;
                                while($a=mysql_fetch_array($data))
                                {
                                echo "<tr>
                                <td>$no</td>
                                <td>$a[nama]</td>
                                <td>$a[kondisi]</td>
                                <td>$a[keterangan]</td>
                                <td>$a[nama_ruang]</td>
                                <td>$a[jumlah]</td>
                                <td>$a[nama_jenis]</td>
                                <td><a href=index.php?page=hapus&table=inventaris&id=$a[id_inventaris]><button class='btn btn-danger btn-circle'><i class='glyphicon glyphicon-trash'></i></button></a>
                                <a href=index.php?page=edit&table=inventaris&id=$a[id_inventaris]><button class='btn btn-info btn-circle'><i class='glyphicon glyphicon-pencil'></i></button></a>
                                </td>
                                </tr>";
                                $no++;
                                }   
                                
                            ?>
    </tbody>

</table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->

      
      <div class='control-sidebar-bg'></div>
    </div><!-- ./wrapper -->

    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>

    <script>
    $(document).ready(function(){
      $('#example').DataTable();
    });
    </script>

  </body>
</html>
