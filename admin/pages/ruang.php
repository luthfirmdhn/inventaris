<?php
include "koneksi.php";
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>AdminLTE 2 | Data Barang</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
</head>

<section class="content-header">
          <h1>
            Data Ruang
          </h1>
</section>

<section class="content">
 <div class="box">
    <div class="box-body">
    <div class="tambah">
        <a href="index.php?page=cu_ruang"><button class="btn btn-success">Tambah Data Ruang</button></a>
    </div></br>
      <table id="example" class="table table-bordered table-striped">
        <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Kode Ruang</th>
                                        <th>Keterangan</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $data=mysql_query("SELECT * FROM ruang");
                                $no=1;
                                while($a=mysql_fetch_array($data))
                                {
                                echo "<tr>
                                <td>$no</td>
                                <td>$a[nama_ruang]</td>
                                <td>$a[kode_ruang]</td>
                                <td>$a[ket_ruang]</td>
                                <td><a href=index.php?page=hapus&table=ruang&id=$a[id_ruang]><button class='btn btn-danger btn-circle'><i class='glyphicon glyphicon-trash'></i></button></a>
                                <a href=index.php?page=cu_ruang&table=ruang&id=$a[id_ruang]><button class='btn btn-info btn-circle'><i class='glyphicon glyphicon-pencil'></i></button></a>
                                </td>
                                </tr>";
                                $no++;
                                }   
                                
                            ?>
    </tbody>

</table>

</div>
</div>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready(function(){
      $('#example').DataTable();
    });
    </script>
        </section>
</html>