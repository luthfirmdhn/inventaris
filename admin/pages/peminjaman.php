  <?php
include "koneksi.php";
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>AdminLTE 2 | Data Barang</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

</head>

<section class="content-header">
          <h1>
            Peminjaman
          </h1>
</section>

<section class="content">
 <div class="box">
    <div class="box-body">

      <div class="col-md-6">
        <form action="" method="post">
          <div class="form-group">
            <label>Nama Peminjam</label>
            <input type="text" class="form-control" name="id_petugas" value="<?php echo $_SESSION['nama_petugas'] ?>" readonly><br></p>
            <label>Nama Barang</label>
            <select name="id_inventaris" class="form-control" required>
              <option value="" selected disabled>- Pilih Barang -</option>
              <?php
                $sql = mysql_query("SELECT * FROM inventaris");
                while ($data=mysql_fetch_array($sql)){
                  echo "<option value=$data[id_inventaris]> $data[nama]</option>";
                }
              ?>
            </select><br></p>
            <label>Jumlah</label>
            <input type="number" class="form-control" name="jumlah" placeholder="Jumlah" required/><br></p>
            <input type="submit" name="pinjam" class="btn btn-success" value="Pinjam" />
            <input type="submit" name="cancel" class="btn btn-primary" value="Cancel" />
          </div>
        </form>
      </div>


<?php
  if(isset($_POST['cancel']))
    echo"<script>window.location.assign('index.php?page=peminjaman')</script>";
?>
  <?php

if(isset($_POST['pinjam']))
{

$id_petugas = $_SESSION['id_petugas'];
$kode_peminjaman = "KD".Date("dmy");
$jumlah = $_POST['jumlah'];
$tgl_pinjam = Date("Y-m-d");
$status_peminjaman = "Sedang Dipinjam";
$input = mysql_query("INSERT INTO peminjaman (tgl_pinjam,status_peminjaman,kode_peminjaman,id_petugas) VALUES('$tgl_pinjam','$status_peminjaman','$kode_peminjaman','$id_petugas')");
  if($input){
    echo"<script>window.location.assign('index.php?page=peminjaman')</script>";
  }else{
    echo mysql_error();
  }
}
?>
</div>
</div>
</section>


<section class="content">
 <div class="box">
    <div class="box-body">
      <table id="example" class="table table-bordered table-striped">
        <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Peminjaman</th>
                                        <th>Nama Peminjam</th>
                                        <th>Tanggal Pinjam</th>
                                        <th>Tanggal Kembali</th>
                                        <th>Status Peminjaman</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $data=mysql_query("SELECT * FROM peminjaman p JOIN petugas s ON p.id_petugas=s.id_petugas");
                                $no=1;
                                while($a=mysql_fetch_array($data))
                                {
                                echo "<tr>
                                <td>$no</td>
                                <td>$a[kode_peminjaman]</td>
                                <td>$a[nama_petugas]</td>
                                <td>$a[tgl_pinjam]</td>
                                <td>$a[tgl_kembali]</td>
                                <td>$a[status_peminjaman]</td>
                                <td><a href=#><button class='btn btn-danger btn-circle'>Kembalikan</i></button></a>
                                </td>
                                </tr>";
                                $no++;
                                }   
                                
                            ?>
    </tbody>

</table>

</div>
</div>
</section>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>

    <script>
    $(document).ready(function(){
      $('#example').DataTable();
    });
    </script>
</html>