<?php
include "koneksi.php";
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>AdminLTE 2 | Data Barang</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

</head>

<section class="content-header">
          <h1>
            Data Pegawai
          </h1>
</section>

<section class="content">
 <div class="box">
    <div class="box-body">
        <div class="tambah">
        <a href="index.php?page=cu_pegawai"><button class="btn btn-success">Tambah Pegawai</button></a>
    </div></br>
      <table id="example" class="table table-bordered table-striped">
        <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Pegawai</th>
                                        <th>NIP</th>
                                        <th>Alamat</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $data=mysql_query("SELECT * FROM pegawai");
                                $no=1;
                                while($a=mysql_fetch_array($data))
                                {
                                echo "<tr>
                                <td>$no</td>
                                <td>$a[nama_pegawai]</td>
                                <td>$a[nip]</td>
                                <td>$a[alamat]</td>
                                <td><a href=index.php?page=hapus&table=pegawai&id=$a[id_pegawai]><button class='btn btn-danger btn-circle'><i class='glyphicon glyphicon-trash'></i></button></a>
                                <a href=index.php?page=cu_pegawai&table=pegawai&id=$a[id_pegawai]><button class='btn btn-info btn-circle'><i class='glyphicon glyphicon-pencil'></i></button></a>
                                </td>
                                </tr>";
                                $no++;
                                }   
                                
                            ?>
    </tbody>

</table>

</div>
</div>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready(function(){
      $('#example').DataTable();
    });
    </script>
        </section>
</html>